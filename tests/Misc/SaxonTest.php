<?php

namespace App\Tests\Misc;

use PHPUnit\Framework\TestCase;

class SaxonTest extends TestCase
{
    /**
     * A test that fails with Saxon.
     */
    public function testFails(): void
    {
        $processor = new \Saxon\SaxonProcessor();
        $input = file_get_contents(__DIR__.'/../../input.xml');
        $xdmDoc = $processor->parseXmlFromString($input);
        $processor3 = $processor->newXslt30Processor();
        $compiled = $processor3->compileFromString(
            file_get_contents(__DIR__.'/../../template.xslt')
        );
        $result = $compiled->transformToString($xdmDoc);

        // This will not work
        $this->assertTrue(true);

        $this->assertNotEmpty($result);
    }

    /**
     * A test that works without Saxon.
     */
    public function testControlWorks(): void
    {
        $this->assertTrue(true);
    }

    public function setUp(): void
    {
    }
}
