<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <title>Books</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="catalog">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="book">
        <p>
            Title:
            <xsl:value-of select="title"/>
        </p>
    </xsl:template>
</xsl:stylesheet>