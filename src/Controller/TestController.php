<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{

    // This is a non-working route, triggered by the use of SaxonProcessor
    // and the creation of a Response.
    // This works too with PHPUnit.
    #[Route('/test', name: 'app_test')]
    public function index(): Response
    {
        $processor = new \Saxon\SaxonProcessor();
        $input = file_get_contents(__DIR__.'/../../input.xml');
        $xdmDoc = $processor->parseXmlFromString($input);
        $processor3 = $processor->newXslt30Processor();
        $compiled = $processor3->compileFromString(
            file_get_contents(__DIR__.'/../../template.xslt')
        );
        $args = [];
        $result = $compiled->transformToString($xdmDoc);

        // Below is the error trigger.
        // If you add 'echo $result; exit;' before this line, the transformed result will be shown correctly.
        $response = new Response($result);

        // Never reached.
        return $response;
    }

    // This is a working route.
    #[Route('/test2', name: 'app_test2')]
    public function index2(): Response
    {
        $result = 'Hellow World';
        $response = new Response($result);

        return $response;
    }
}
