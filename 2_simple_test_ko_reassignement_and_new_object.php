<?php

// Registers a new loader.
spl_autoload_register(function ($class_name) {
    echo sprintf(">> Autoloading '%s' <<", $class_name);
    require_once __DIR__ . '/SimpleClass.php';
});

$processor = new \Saxon\SaxonProcessor();
$input = file_get_contents(__DIR__.'/input.xml');
$xdmDoc = $processor->parseXmlFromString($input);
$processor = $processor->newXslt30Processor();

$compiled = $processor->compileFromString(
    file_get_contents(__DIR__.'/template.xslt')
);
$args = [];
$result = $compiled->transformToString($xdmDoc);

echo $result;

echo "\n";
# There will be a segmentation fault just there.
$o = new Dummy\Ns1\SimpleClass('Hello');
echo "\n";
$o->write();
echo "\n";
echo var_dump($o);
