<?php

// Registers a new loader.
spl_autoload_register(function ($class_name) {
    echo sprintf(">> Autoloading '%s' <<", $class_name);
    require_once __DIR__ . '/SimpleClass.php';
});

echo "\n";
$o = new Dummy\Ns1\SimpleClass('Hello');
echo "\n";
$o->write();
echo "\n";
echo var_dump($o);
