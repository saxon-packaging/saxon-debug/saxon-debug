<?php

namespace Dummy\Ns1;

class SimpleClass
{
    public function __construct(
        protected string $p1 = ''
    ) {
    }

    public function write(): void {
        echo $this->p1;
    }
}