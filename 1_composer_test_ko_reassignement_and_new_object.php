<?php

require_once __DIR__ . '/vendor/autoload.php';

$processor = new \Saxon\SaxonProcessor();
$input = file_get_contents(__DIR__.'/input.xml');
$xdmDoc = $processor->parseXmlFromString($input);
# Reassignment.
$processor = $processor->newXslt30Processor();

$compiled = $processor->compileFromString(
    file_get_contents(__DIR__.'/template.xslt')
);
$args = [];
$result = $compiled->transformToString($xdmDoc);

echo $result;

echo "\n";
$r = new Symfony\Component\HttpFoundation\Response();
echo var_dump($r);